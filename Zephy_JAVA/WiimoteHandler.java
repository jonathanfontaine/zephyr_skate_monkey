package roomDestruction.Handlers;

import roomDestruction.Main;
import roomDestruction.Room.Camera;
import wiiusej.WiiUseApiManager;

import wiiusej.wiiusejevents.physicalevents.ExpansionEvent;
import wiiusej.wiiusejevents.physicalevents.IREvent;
import wiiusej.wiiusejevents.physicalevents.MotionSensingEvent;
import wiiusej.wiiusejevents.physicalevents.WiimoteButtonsEvent;
import wiiusej.wiiusejevents.utils.WiimoteListener;
import wiiusej.wiiusejevents.wiiuseapievents.ClassicControllerInsertedEvent;
import wiiusej.wiiusejevents.wiiuseapievents.ClassicControllerRemovedEvent;
import wiiusej.wiiusejevents.wiiuseapievents.DisconnectionEvent;
import wiiusej.wiiusejevents.wiiuseapievents.GuitarHeroInsertedEvent;
import wiiusej.wiiusejevents.wiiuseapievents.GuitarHeroRemovedEvent;
import wiiusej.wiiusejevents.wiiuseapievents.NunchukInsertedEvent;
import wiiusej.wiiusejevents.wiiuseapievents.NunchukRemovedEvent;
import wiiusej.wiiusejevents.wiiuseapievents.StatusEvent;

	Wiimote[] w_WiiMotes =  WiiUseApiManager.getWiimotes(1, true);
    	Wiimote w_WiiMote = w_WiiMotes[0];
    	w_WiiMote.addWiiMoteEventListeners(new WiimoteHandler());
    	w_WiiMote.activateMotionSensing();

public class WiimoteHandler implements WiimoteListener{

    public void onButtonsEvent(WiimoteButtonsEvent ai_Event) {
       
        if (ai_Event.isButtonAPressed()){
            WiiUseApiManager.shutdown();
        }
        
        if(ai_Event.isButtonRightHeld())
        {
        	   Camera.m_Camera1.ry.setAngle(Camera.m_Camera1.ry.getAngle() - 5.0);  
              // Camera.m_Camera1.rx.setAngle(Camera.m_Camera1.rx.getAngle() + 2.0);
        }
        
        if(ai_Event.isButtonLeftHeld())
        {
        	   Camera.m_Camera1.ry.setAngle(Camera.m_Camera1.ry.getAngle() + 5.0);  
        }
          
        if(ai_Event.isButtonUpHeld())
        {
        	 Camera.m_Camera.setTranslateZ(Camera.m_Camera.getTranslateZ() + 50);
        }
        
        if(ai_Event.isButtonDownHeld())
        {
        	 Camera.m_Camera.setTranslateZ(Camera.m_Camera.getTranslateZ() - 50);
        }    
    }

    public void onMotionSensingEvent(MotionSensingEvent ai_Event) {
    
	   if( ai_Event.getRawAcceleration().getX() > 220)
	   {
		   Main.m_Ball.Throw((int)Camera.m_Camera1.ry.getAngle(), (int) ai_Event.getRawAcceleration().getY());
	   }
    }
    
    public void onIrEvent(IREvent arg0) {
    }

    public void onExpansionEvent(ExpansionEvent arg0) {
       // System.out.println(arg0);
    }

    public void onStatusEvent(StatusEvent arg0) {
       // System.out.println(arg0);
    }

    public void onDisconnectionEvent(DisconnectionEvent arg0) {
       // System.out.println(arg0);
    }

    public void onNunchukInsertedEvent(NunchukInsertedEvent arg0) {
       // System.out.println(arg0);
    }

    public void onNunchukRemovedEvent(NunchukRemovedEvent arg0) {
       // System.out.println(arg0);
    }

	@Override
	public void onClassicControllerInsertedEvent(ClassicControllerInsertedEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onClassicControllerRemovedEvent(ClassicControllerRemovedEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onGuitarHeroInsertedEvent(GuitarHeroInsertedEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onGuitarHeroRemovedEvent(GuitarHeroRemovedEvent arg0) {
		// TODO Auto-generated method stub
		
	}
}
