package display;

import javafx.animation.AnimationTimer;
import javafx.application.Platform;
import javafx.scene.control.Label;
import objects.Monkey;
import processing.box.ProcessingBox;
public class GameLoop extends AnimationTimer{
	float i=0;
	Xform monkeyForm, scene;
	Monkey zephyr;
	int blockSize, flag = 0;
	ProcessingBox pb;
	static long m_startTime;
	Label lblTimer;
	boolean timed;
	
	
	public GameLoop(Xform monkeyForm, Xform scene, Monkey zephyr, int blockSize, Label timer)
	{
		this.monkeyForm = monkeyForm;
		this.scene = scene;
		this.zephyr = zephyr;
		this.blockSize = blockSize;
		this.start();
		timed = false;
		lblTimer = timer;
	}
	
	public void updateMonkeyPosition()
	{
		if(zephyr._visualRemove != null)
		{
			scene.getChildren().remove(zephyr._visualRemove);
			zephyr._visualRemove = null;
		}
		
		monkeyForm.setTranslateZ(-zephyr.posX*blockSize); //Z = -X du physic engine
		monkeyForm.setTranslateX(zephyr.posY*blockSize); //X = Y du physic engine
		monkeyForm.getChildren().get(1).setTranslateY(-zephyr.posZ*blockSize); //Y = Z du physic engine
		
		gUtility.matrixRotateNode(monkeyForm.getChildren().get(1), 
				zephyr.angleFront, zephyr.angleSide, zephyr.angleOrientation);
		
		flag = 0;

		if(zephyr.posY < 2 && zephyr.posY > 1 && !timed)
		{
			m_startTime = System.currentTimeMillis();
			lblTimer.setText("000:00");
		}
		else
		{
			timed = true;
			
			long w_time = System.currentTimeMillis() - m_startTime;
			
			String t = Long.toString(w_time/10);
			String mili,sec;
			mili = t.substring(t.length()-2,t.length());
			if(t.length() == 2)
			{
				sec = "000";
			}
			else if(t.length() == 3)
			{
				sec = "00" + t.substring(t.length()-3,t.length()-2);
			}
			else if(t.length() == 4)
			{
				sec = "0" + t.substring(t.length()-4,t.length()-2);
			}
			else
			{
				sec = t.substring(t.length()-5,t.length()-2);
			}
			if(zephyr.alive && !zephyr.win)
			{
				lblTimer.setText(sec + ":" + mili );
			}
		}	
	}
	public void handle(long now) {
		// TODO Auto-generated method stub
	//	System.out.println("INSIDE ANIMATION TIMER" + i);
		//this.stop();
		++i;
		if(flag == 0){
			flag = 1;
			updateMonkeyPosition();}
		//monkeyForm.setTranslateX(i);
//		 Platform.runLater(() -> {
//		        try {
//		            //an event with a button maybe
//		        	
//		        } catch (Exception ex) {
//		            
//		        }
//		    });
		//Box b = new Box(1,1,1);
	//	GameForm.getChildren().add(b);
	 //  b.setTranslateX(1+i);
	   
		
	}
		//build again

	public static void resetTimer() 
	{
		m_startTime = System.currentTimeMillis();
	}

}
