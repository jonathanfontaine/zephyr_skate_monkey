package display;

import javafx.scene.Node;

public class gUtility {
	public static void matrixRotateNode(Node n, double alf, double bet, double gam){
	    double A11=Math.cos(alf)*Math.cos(gam);
	    double A12=Math.cos(bet)*Math.sin(alf)+Math.cos(alf)*Math.sin(bet)*Math.sin(gam);
	    double A13=Math.sin(alf)*Math.sin(bet)-Math.cos(alf)*Math.cos(bet)*Math.sin(gam);
	    double A21=-Math.cos(gam)*Math.sin(alf);
	    double A22=Math.cos(alf)*Math.cos(bet)-Math.sin(alf)*Math.sin(bet)*Math.sin(gam);
	    double A23=Math.cos(alf)*Math.sin(bet)+Math.cos(bet)*Math.sin(alf)*Math.sin(gam);
	    double A31=Math.sin(gam);
	    double A32=-Math.cos(gam)*Math.sin(bet);
	    double A33=Math.cos(bet)*Math.cos(gam);

	    double d = Math.acos((A11+A22+A33-1d)/2d);
	    if(d!=0d){
	        double den=2d*Math.sin(d);
	        javafx.geometry.Point3D p= new javafx.geometry.Point3D((A32-A23)/den,(A13-A31)/den,(A21-A12)/den);
	        n.setRotationAxis(p);
	        n.setRotate(Math.toDegrees(d));                    
	    }
	}
}
