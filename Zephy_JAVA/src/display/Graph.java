/*
 * Copyright (c) 2013, 2014 Oracle and/or its affiliates.
 * All rights reserved. Use is subject to license terms.
 *
 * This file is available and licensed under the following license:
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *  - Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the distribution.
 *  - Neither the name of Oracle nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package display;

import java.awt.Point;
import java.io.File;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.JOptionPane;
import javax.xml.bind.annotation.XmlElementDecl.GLOBAL;

import com.interactivemesh.jfx.importer.col.ColModelImporter;
import com.sun.xml.internal.fastinfoset.algorithm.BuiltInEncodingAlgorithm.WordListener;
import com.sun.xml.internal.ws.api.streaming.XMLStreamWriterFactory.Zephyr;

import Controller.BBoard_State;
import Controller.ControllerVisualizer;
import Controller.IController;
import Controller.IHM_ControllerSimu_P;
import Controller.WiiMoteController;
import audio.AudioPlayer;
import common.Pair;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Point3D;
import javafx.scene.AmbientLight;
import javafx.scene.PointLight;
import javafx.scene.DepthTest;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.PerspectiveCamera;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.Box;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import maps.Case;
import maps.Map;
import maps.MapPortionLine;
import maps.MapSegment;
import maps.eElement;
import objects.Monkey;
import physicsEngine.PhysicsEngine;
import sun.security.util.PendingException;

public class Graph extends Application {
    // PRINTING PROPERTIES
    final Group root = new Group();
    final Xform axisGroup = new Xform();
    Xform world = new Xform();
    final PerspectiveCamera camera = new PerspectiveCamera(true);
    final Xform cameraXform = new Xform();
    final Xform cameraXform2 = new Xform();
    final Xform cameraXform3 = new Xform();
    final Xform skatingMonkey = new Xform();
    private static final double CAMERA_INITIAL_DISTANCE = 120;
    private static final double CAMERA_INITIAL_X_ANGLE = -21;
    private static final double CAMERA_INITIAL_Y_ANGLE = 90;
    private static final double CAMERA_NEAR_CLIP = 200;
    private static final double CAMERA_FAR_CLIP = 10000.0;
    private static final double AXIS_LENGTH = 1000.0;
    private static final double CONTROL_MULTIPLIER = 0.1;
    private static final double SHIFT_MULTIPLIER = 10.0;
    private static final double MOUSE_SPEED = 0.1;
    private static final double ROTATION_SPEED = 2.0;
    private static final double TRACK_SPEED = 0.3;
    //Parameters rgarding BLOCK dimension
    private static final String DAE_FILENAME = ("model.dae"); 
    private static final String DAE_FILENAME3 = ("model3.dae");
    
    
    private Stage m_primaryStage;
    
    
    Button[] m_menuButtons;
    
    static GameLoop gL;
    
    static ArrayList<Pair<Pair<Integer,Integer>,Group>> w_visualBanana; 
   
    static Map myMap;
    static Monkey zephyr;
    static BBoard_State myBBoard;
    static PhysicsEngine pEgine;

    //PhongMaterials
	  PhongMaterial arrival;
	  PhongMaterial border ;
	  PhongMaterial fire ;
	  PhongMaterial grass;
	  PhongMaterial wheat;
	  PhongMaterial sand;
	  PhongMaterial hole;
	  PhongMaterial wood;
	  PhongMaterial water;
	  PhongMaterial bridge;
	  PhongMaterial rock;
	  PhongMaterial rockBlock;
	  PhongMaterial ground;
	  Label wheatLbl;

    // CONTROL PROPERTIES
    double mousePosX;
    double mousePosY;
    double mouseOldX;
    double mouseOldY;
    double mouseDeltaX;
    double mouseDeltaY;
    
    
    Label timer;
    
    // Models Group
    ArrayList<Group> g_modelsGroup;
   
    // THREAD COMPUTING PROPERTIES
    int i = 0;
    // Generate map
    static Group loadNodes(String filename) {
        File file = new File(filename);
        ColModelImporter importer = new ColModelImporter();
        importer.read(file);
        Node[] nodes = importer.getImport();
        Group modelGroup = new Group(nodes);
        return modelGroup;
    }
    //MODELS
    private void buildModels() {
    	g_modelsGroup = new ArrayList<Group>();
    	Group g1 = loadNodes(DAE_FILENAME);
    	Group g3 = loadNodes(DAE_FILENAME3);
       		
      	
  	g1.setScaleY(0.5);
   	g1.setScaleZ(0.5);
  	g1.setScaleX(0.5);
   	g1.setTranslateY(-50);
   	g1.setTranslateZ( 10 );
   
   	
   	g3.setScaleY(3);
   	g3.setScaleZ(3);
  	g3.setScaleX(3);
  	g3.setTranslateX(-40);
  	g3.setTranslateY(-6);
  	g3.setTranslateZ(-80);
   	gUtility.matrixRotateNode(g3,
			0,   //x
			0,   //y
			1.57); //z
   	Group singerie = new Group();
   	singerie.getChildren().clear();
   	singerie.getChildren().addAll(g1,g3);
   	skatingMonkey.getChildren().addAll(singerie);
    	//world.getChildren().addAll(g1,g3);
   	world.getChildren().add(skatingMonkey);
//   	world.getChildren().add(g_treeGroup);
   	
   	// TIMER
    timer = new Label("Timer");
   	
   	timer.setScaleY(3);
   	timer.setScaleZ(3);
   	timer.setScaleX(3);
   	timer.setTranslateX(0);
   	timer.setTranslateY(-220);
   	timer.setTranslateZ(-120);
   	
   	gUtility.matrixRotateNode(timer,
			0,   //x
			0,   //y
			-1.57); //z
   	
   	skatingMonkey.getChildren().add(timer);
    }
    
    // CAMERA
    private void buildCamera() {
        root.getChildren().add(cameraXform);
        cameraXform.getChildren().add(cameraXform2);
        cameraXform2.getChildren().add(cameraXform3);
        cameraXform3.getChildren().add(camera);
        
        PointLight light = new PointLight();
        cameraXform.getChildren().add(light);
       
        camera.setNearClip(CAMERA_NEAR_CLIP);
        camera.setFarClip(CAMERA_FAR_CLIP);
        camera.setTranslateY(-CAMERA_INITIAL_DISTANCE);
        camera.setFieldOfView(35);
        cameraXform.ry.setAngle(CAMERA_INITIAL_Y_ANGLE);
        cameraXform.rx.setAngle(CAMERA_INITIAL_X_ANGLE);
        cameraXform.setTranslate(-500, -150, 0);
        skatingMonkey.getChildren().add(cameraXform);
    }

    // MOUSE
    private void handleMouse(Scene scene, final Node root) {
        scene.setOnMousePressed(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {
                mousePosX = me.getSceneX();
                mousePosY = me.getSceneY();
                mouseOldX = me.getSceneX();
                mouseOldY = me.getSceneY();      
               
            }
        });
        scene.setOnMouseDragged(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {
                mouseOldX = mousePosX;
                mouseOldY = mousePosY;
                mousePosX = me.getSceneX();
                mousePosY = me.getSceneY();
                mouseDeltaX = (mousePosX - mouseOldX);
                mouseDeltaY = (mousePosY - mouseOldY);

                double modifier = 1.0;

                if (me.isControlDown()) {
                    modifier = CONTROL_MULTIPLIER;
                }
                if (me.isShiftDown()) {
                    modifier = SHIFT_MULTIPLIER;
                }    
                if (me.isPrimaryButtonDown()) {
                    cameraXform.ry.setAngle(cameraXform.ry.getAngle() - mouseDeltaX*MOUSE_SPEED*modifier*ROTATION_SPEED); 
                    cameraXform.rx.setAngle(cameraXform.rx.getAngle() + mouseDeltaY*MOUSE_SPEED*modifier*ROTATION_SPEED); 
                }
                else if (me.isSecondaryButtonDown()) {
                    double z = camera.getTranslateZ();
                    double newZ = z + mouseDeltaX*MOUSE_SPEED*modifier;
                    camera.setTranslateZ(newZ);
                   
                }
                else if (me.isMiddleButtonDown()) {
                    cameraXform2.t.setX(cameraXform2.t.getX() + mouseDeltaX*MOUSE_SPEED*modifier*TRACK_SPEED); 
                    cameraXform2.t.setY(cameraXform2.t.getY() + mouseDeltaY*MOUSE_SPEED*modifier*TRACK_SPEED); 
                }
            }
        });
    }
    
    public static void restart()
    {
    	boolean toRestart = false;
    	if(!zephyr.alive || zephyr.win )
    	{
    		toRestart = true;
    	}
    	zephyr.alive = true;
    	zephyr.win = false;
    	GameLoop.resetTimer();
    	pEgine.init();
    	if(toRestart)
    	{
    		pEgine.start();
    	}
    	AudioPlayer.ridin();
    }

    // KEYBOARD
    private void handleKeyboard(Scene scene, final Node root) {
        scene.setOnKeyPressed(new EventHandler<KeyEvent>() {
            public void handle(KeyEvent event) {
                switch (event.getCode()) {
                case M :
                	 //JOptionPane.showMessageDialog(null,new Menu_main(),"Menu Principal", JOptionPane.PLAIN_MESSAGE);
                	 
                	m_menuButtons = new Button[3];
                	m_menuButtons[0] = new Button("Map Test");
                	m_menuButtons[1] = new Button("Map Dev");
                	m_menuButtons[2] = new Button("Monkeyland");
                	m_menuButtons[0].setOnAction(
                	        new EventHandler<ActionEvent>() {
                	            @Override
                	            public void handle(ActionEvent event) {
                	                loadMap("map_test");
                	            }
                	         });
                	m_menuButtons[1].setOnAction(
                	        new EventHandler<ActionEvent>() {
                	            @Override
                	            public void handle(ActionEvent event) {
                	                loadMap("map_dev");
                	            }
                	         });
                	m_menuButtons[2].setOnAction(
                	        new EventHandler<ActionEvent>() {
                	            @Override
                	            public void handle(ActionEvent event) {
                	                loadMap("monkeyland");
                	            }
                	         });
                	
                	
                	
                	
                	final Stage dialog = new Stage();
	                dialog.initModality(Modality.APPLICATION_MODAL);
	                dialog.initOwner(m_primaryStage);
	                VBox dialogVbox = new VBox(5);
	                dialogVbox.getChildren().add(new Text("Menu Cartes :"));
	                dialogVbox.getChildren().add(m_menuButtons[0]);
	                dialogVbox.getChildren().add(m_menuButtons[1]);
	                dialogVbox.getChildren().add(m_menuButtons[2]);
	                
	                Scene dialogScene = new Scene(dialogVbox, 200, 200);
	                dialog.setScene(dialogScene);
	                dialog.show();
                	
                	break;
                case Z:
                    break;
                case X:
                    axisGroup.setVisible(!axisGroup.isVisible());
                    break;
                case V:
                    break;
				default:
					break;
                }
            }
        });
    }

    private void loadMap(String name)
    {
    	pEgine.stop();
    	// vider la map
    	world.getChildren().clear();
    	
    	myMap = new Map(name);
    	zephyr = new Monkey("Le nouveau Zephyr");

        pEgine = new PhysicsEngine(myMap, zephyr, myBBoard);
        pEgine.start();
    	
    	world.getChildren().add(skatingMonkey);
        buildMap(myMap);

        gL = new GameLoop(skatingMonkey,world,zephyr,myMap.blockSize,timer);
        gL.start();
    }
    
    // SCENE
    public static void main(String[] args) {
        launch(args);
    }
   
    // START
    @Override
    public void start(Stage primaryStage) throws InterruptedException {
    	        
    	myBBoard = new BBoard_State();
    	myMap = new Map("monkeyland");
    	zephyr = new Monkey("Le Zephyr");
    	
    	w_visualBanana = new ArrayList<Pair<Pair<Integer,Integer>,Group>>();
        
    	IController w_controller = new WiiMoteController(myBBoard);
    	if (w_controller.autoConnect())
    	{
    		ControllerVisualizer w_cv = new ControllerVisualizer(myBBoard);
    		w_cv.start();
    	}
    	else
    	{
    		System.out.println("ERREUR : impossible de connecter le controller automatiquement.");
    		System.out.println("Passage en mode de contr�le : simulation");
    		w_controller = new IHM_ControllerSimu_P(myBBoard);
    	}
    	pEgine = new PhysicsEngine(myMap, zephyr, myBBoard);
    	pEgine.start();
		
        root.getChildren().add(world);
        root.setDepthTest(DepthTest.ENABLE);
        AmbientLight al = new AmbientLight();
        al.setColor(Color.ALICEBLUE);
        root.getChildren().add(al);
       
        buildPhongMaterials();
        
        buildCamera();
         
   
        buildModels();
        
        
      
        gL = new GameLoop(skatingMonkey,world,zephyr,myMap.blockSize,timer);
       
        Scene scene = new Scene(root, 1024, 768, true);
        scene.setFill(Color.GRAY);
        
        handleKeyboard(scene, world);
        handleMouse(scene, world);
        
        primaryStage.setTitle("Zephyr riding monkey");
        primaryStage.setScene(scene);
        primaryStage.show();
        scene.setCamera(camera);
        
        buildMap(myMap);
        
        AudioPlayer.ridin();
                       
    }
    
    private void generateTree()
    {
    	Group tree = loadNodes("models/tree/model.dae");
    	
    	tree.setScaleY(0.021);
       	tree.setScaleZ(0.021);
      	tree.setScaleX(0.021);
      	tree.setTranslateY(385); // z
     	tree.setTranslateX(-3009); // y
       	tree.setTranslateZ( 1834 ); // X
       	
       	Group w_treeGroup = new Group();
       	w_treeGroup.getChildren().add(tree);
       	
       	g_modelsGroup.add(w_treeGroup);
    }
    
    private void generateBanana()
    {
    	Group banana = loadNodes("models/banana/model.dae");
    	banana.setScaleY(4);
    	banana.setScaleZ(4);
    	banana.setScaleX(4);
    	banana.setTranslateY(95); // z
    	banana.setTranslateX(-35); // y
    	banana.setTranslateZ( -115 ); // X
       	Group w_bananaGroup = new Group();
       	w_bananaGroup.getChildren().add(banana);
       	
       	g_modelsGroup.add(w_bananaGroup);
    }
    
	private void buildPhongMaterials() {
		String w_bp = "textures/MC_Sapix/";
    	File w_f;
    	Image w_im;
		
		arrival = new PhongMaterial();
		w_f = new File(w_bp+"arrival.jpg");
		w_im = new Image(w_f.toURI().toString()); 
    	arrival.setSpecularMap(w_im);
    	arrival.setDiffuseMap(w_im);

		
		border = new PhongMaterial();
		w_f = new File(w_bp+"planks_jungle.png");
		w_im = new Image(w_f.toURI().toString()); 
		border.setSpecularMap(w_im);
		border.setDiffuseMap(w_im);
		
		fire = new PhongMaterial();
		w_f = new File(w_bp+"lava.png");
		w_im = new Image(w_f.toURI().toString()); 
		fire.setSpecularMap(w_im);
		fire.setDiffuseMap(w_im);
		
		ground = new PhongMaterial();
		w_f = new File(w_bp+"dirt.png");
		w_im = new Image(w_f.toURI().toString()); 
		ground.setSpecularMap(w_im);
		ground.setDiffuseMap(w_im);
		  
		grass = new PhongMaterial();
		w_f = new File(w_bp+"grass_top.png");
		w_im = new Image(w_f.toURI().toString()); 
		grass.setSpecularMap(w_im);
		grass.setDiffuseMap(w_im);
		grass.setDiffuseColor(Color.GREENYELLOW);
		grass.setSpecularColor(Color.GREENYELLOW)
		;
		
		wheat = new PhongMaterial();
		w_f = new File(w_bp+"wheat_stage_3.png");
		w_im = new Image(w_f.toURI().toString());
		wheat.setSpecularMap(w_im);
		wheat.setDiffuseMap(w_im);
		
		wheatLbl = new Label();
		wheatLbl.setGraphic(new ImageView(w_im));

	 
		wood = new PhongMaterial();
		w_f = new File(w_bp+"log_oak.png");
		w_im = new Image(w_f.toURI().toString()); 
		wood.setSpecularMap(w_im);
		wood.setDiffuseMap(w_im);
        
    	water = new PhongMaterial();
    	w_f = new File(w_bp+"water.jpg");
    	w_im = new Image(w_f.toURI().toString());
    	water.setSpecularMap(w_im);
    	water.setDiffuseMap(w_im);
        
        sand = new PhongMaterial();
        w_f = new File(w_bp+"sand.png");
    	w_im = new Image(w_f.toURI().toString());
    	sand.setSpecularMap(w_im);
    	sand.setDiffuseMap(w_im);
    	
    	bridge = new PhongMaterial();
        w_f = new File(w_bp+"planks_oak.png");
    	w_im = new Image(w_f.toURI().toString());
    	bridge.setSpecularMap(w_im);
    	bridge.setDiffuseMap(w_im);
    	
    	rock = new PhongMaterial();
        w_f = new File(w_bp+"cobblestone.png");
    	w_im = new Image(w_f.toURI().toString());
    	rock.setSpecularMap(w_im);
    	rock.setDiffuseMap(w_im);
    	
    	rockBlock = new PhongMaterial();
    	w_f = new File(w_bp+"bedrock.png");
    	w_im = new Image(w_f.toURI().toString());
    	rockBlock.setSpecularMap(w_im);
    	rockBlock.setDiffuseMap(w_im);
	}
	private void buildMap(Map gameMap) {
		buildMapSegment(gameMap.currentSegment,gameMap.blockSize);
	}
	
	private void buildMapSegment(MapSegment mSeg, int blockSize)
	{
		int done,total;
		done = 0;
		total = mSeg.portions.size();
		int currentDepth = 0;
		for(Pair<MapPortionLine, Integer> portion : mSeg.portions)
		{
			System.out.println(done++ + " / " + total);
			int totalDepthSegment = portion.second;
			for(i = 0; i< totalDepthSegment ; i++)
			{
				buildMapPortionLine(portion.first, currentDepth, blockSize);
				currentDepth++;
			}
		}
	}
	
	private void buildMapPortionLine(MapPortionLine mpl, int depth, int blockSize)
	{
		for(int i = 0; i < mpl.size; i++)
		{
			addBlock(mpl.cases[i], i*blockSize , depth*blockSize, blockSize);
		}
	}
	private void addBlock(Case givenCase, int x, int y, int blockSize)
	{
		Box b = null;
		switch (givenCase.element)
		{
		case border :  
			b = new Box(blockSize,90,blockSize);
			b.setMaterial(border);
			break;
		case grass : 
			b = new Box(blockSize,1,blockSize);
			b.setMaterial(grass);
			if(Math.random() < 0.15)
			{
				Box g = new Box(0,30,blockSize/ (Math.random()*10 +1) ) ;
				g.setMaterial(wheat);
				g.setTranslateZ(-x +  ((Math.random()-0.5)*blockSize) ) ;
				g.setTranslateX(y +  ((Math.random()-0.5)*blockSize) );
				g.setTranslateY(-(Math.random()*15));
				world.getChildren().add(g);
			}
			break;
		case sand : 
			b = new Box(blockSize,1,blockSize);
			b.setMaterial(sand);
			
			break;
		case fire : 
			 b = new Box(blockSize,0.7,blockSize);
			b.setMaterial(fire);
			break;
		case wood : 
			b = new Box(blockSize,70,blockSize);
			b.setMaterial(wood);
			break;
		case arrival :
			b = new Box(blockSize,1,blockSize);
			b.setMaterial(arrival);
			break;
		case water :
			b = new Box(blockSize,1,blockSize);
			b.setMaterial(water);
			break;
		case ground :
			b = new Box(blockSize,1,blockSize);
			b.setMaterial(ground);
			break;
		case none : 
			return;
		case hole : 
			return;
		case tree :
			generateTree();
			Group w_tree = new Group(g_modelsGroup.get(g_modelsGroup.size()-1));
			world.getChildren().add(w_tree);
			w_tree.setTranslateZ(-x);
			w_tree.setTranslateX(y);
			b = new Box(blockSize,1,blockSize);
			b.setMaterial(grass);
			break;
		case bridge :
			b = new Box(blockSize,10,blockSize);
			b.setMaterial(bridge);
			break;
		case rock :
			b = new Box(blockSize,1,blockSize);
			b.setMaterial(rock);
			break;
		case rockBlock :
			b = new Box(blockSize,90,blockSize);
			b.setMaterial(rockBlock);
			break;
		default : 
			return;
		}
		b.setTranslateZ(-x);
		b.setTranslateX(y);
		world.getChildren().add(b);
		
		// OBJECTS
		switch (givenCase.object.type)
		{
			case banana :
				generateBanana();
				Group w_banana = new Group(g_modelsGroup.get(g_modelsGroup.size()-1));
				world.getChildren().add(w_banana);
				w_banana.setTranslateZ(-x);
				w_banana.setTranslateX(y);
				givenCase.object.visualObject = w_banana;
				break;
			default : 
				return;
		}

	}

}