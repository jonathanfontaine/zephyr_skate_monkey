package Controller;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseEvent;

import javax.swing.JFrame;
import javax.swing.JPanel;

import maps.Map;
import objects.Monkey;

public class ControllerVisualizer {
	// ATTRIBUTES
    private static JFrame graphFrame;
    private static JPanel graph;
	private BBoard_State m_bbs;
	private boolean m_stateOn;
	
	public ControllerVisualizer(BBoard_State ai_bbs)
	{
		m_bbs = ai_bbs;
		m_stateOn = false;
		// init IHM components
		graphFrame = new JFrame();
        graphFrame.setTitle("Balance Board Vizualizer");
        graphFrame.setSize(600, 800);
        graphFrame.setResizable(false);
        
        graph = new JPanel()
        {
            public void paintComponent(Graphics graphics)
            {
                graphics.clearRect(0, 0, 600, 800);
                graphics.fillRect(0, 0, 600, 800);
                graphics.setColor(Color.WHITE);
                graphics.drawLine(0, 400, 600, 400);
                graphics.drawLine(300, 0, 300, 800);
                
                graphics.setColor(Color.RED);
                graphics.fillOval((int)( m_bbs.xWeight * 600+300-15), (int)(m_bbs.yWeight * 800+400-15), 30, 30);
            }
        };
        
        graph.setDoubleBuffered(true);
        graphFrame.add(graph);
        graphFrame.setVisible(true);
        graphFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	public void start()
	{
		m_stateOn = true;
		new Thread(() -> {
		    try {
		    	while(m_stateOn)
		    	{
		    		updateInfos();
		    		Thread.sleep(33);
		    	}
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}).start();
	}
	public void stop()
	{
		m_stateOn = false;
	}
	
	private void updateInfos()
	{
		graph.repaint();
	}
	
	public static void main(String[] args) {
		BBoard_State bbs = new BBoard_State();
	}
}
