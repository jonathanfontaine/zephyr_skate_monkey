package Controller;

import javax.swing.JOptionPane;

import audio.AudioPlayer;
import display.Graph;
import display.Menu_main;
import sun.applet.Main;
import wiiusej.WiiUseApi;
import wiiusej.WiiUseApiManager;
import wiiusej.Wiimote;
import wiiusej.wiiusejevents.physicalevents.ExpansionEvent;
import wiiusej.wiiusejevents.physicalevents.IREvent;
import wiiusej.wiiusejevents.physicalevents.MotionSensingEvent;
import wiiusej.wiiusejevents.physicalevents.WiimoteButtonsEvent;
import wiiusej.wiiusejevents.utils.WiimoteListener;
import wiiusej.wiiusejevents.wiiuseapievents.ClassicControllerInsertedEvent;
import wiiusej.wiiusejevents.wiiuseapievents.ClassicControllerRemovedEvent;
import wiiusej.wiiusejevents.wiiuseapievents.DisconnectionEvent;
import wiiusej.wiiusejevents.wiiuseapievents.GuitarHeroInsertedEvent;
import wiiusej.wiiusejevents.wiiuseapievents.GuitarHeroRemovedEvent;
import wiiusej.wiiusejevents.wiiuseapievents.NunchukInsertedEvent;
import wiiusej.wiiusejevents.wiiuseapievents.NunchukRemovedEvent;
import wiiusej.wiiusejevents.wiiuseapievents.StatusEvent;

public class WiiMoteController implements WiimoteListener, IController {

	Wiimote m_wiiMote;
	BBoard_State m_bbs;
	float m_maxPitch, m_maxRoll,m_minPicth,m_minRoll,m_minJumpGForce;
	
	public boolean autoConnect()
	{
		Wiimote[] w_wiiMotes = WiiUseApiManager.getWiimotes(1, true);
		if(WiiUseApiManager.getNbConnectedWiimotes() <= 0)
		{
			return false;
		}
		m_wiiMote = w_wiiMotes[0];
		m_wiiMote.addWiiMoteEventListeners(this);
		m_wiiMote.activateMotionSensing();
		m_wiiMote.setOrientationThreshold(0);
		return true;
	}
	
	public WiiMoteController(BBoard_State ai_bbs)
	{
		m_bbs = ai_bbs;
		m_maxPitch = 45f;
		m_maxRoll = 45;
		m_minPicth = 0f;
		m_minRoll = 0f;
		m_minJumpGForce = 1.85f;
	}

	@Override
	public void onButtonsEvent(WiimoteButtonsEvent arg0) {
		if(arg0.isButtonHomeJustReleased())
		{
			Graph.restart();
		}
		if(arg0.isButtonAJustPressed())
		{
			m_bbs.jumping = true;
			AudioPlayer.jump();
		}
	
	}

	@Override
	public void onClassicControllerInsertedEvent(ClassicControllerInsertedEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onClassicControllerRemovedEvent(ClassicControllerRemovedEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onDisconnectionEvent(DisconnectionEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onExpansionEvent(ExpansionEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onGuitarHeroInsertedEvent(GuitarHeroInsertedEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onGuitarHeroRemovedEvent(GuitarHeroRemovedEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onIrEvent(IREvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onMotionSensingEvent(MotionSensingEvent arg0) {
		
		float w_xAxis =arg0.getOrientation().getARoll();
		float w_yAxis =arg0.getOrientation().getAPitch();
		// ROLL
		// manage max values
		if( Math.abs(w_xAxis)>m_maxRoll)
		{
			w_xAxis = m_maxRoll * Integer.signum((int)w_xAxis);
		}
		//manage min values
		if( Math.abs(w_xAxis)<m_minRoll)
		{
			w_xAxis = 0;
		}
		// compute to percentage of max value (O.5 max)
		w_xAxis /= m_maxRoll * 2;
		// PITCH
		// manage max values
		if( Math.abs(w_yAxis)>m_maxPitch)
		{
			w_yAxis = m_maxPitch * Integer.signum((int)w_yAxis);
		}
		//manage min values
		if( Math.abs(w_yAxis)<m_minPicth)
		{
			w_yAxis = 0;
		}
		// compute to percentage of max value (O.5 max)
		w_yAxis /= m_maxPitch * 2;
		
		// JUMP
		float w_zGravity = arg0.getGforce().getZ();
		if(w_zGravity>= m_minJumpGForce)
		{
			m_bbs.jumping = true;
			AudioPlayer.jump();
		}
		
		// affect to BB state
		m_bbs.xWeight = w_xAxis;
		m_bbs.yWeight = -w_yAxis;
	}

	@Override
	public void onNunchukInsertedEvent(NunchukInsertedEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onNunchukRemovedEvent(NunchukRemovedEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStatusEvent(StatusEvent arg0) {
		// TODO Auto-generated method stub
		
	}

}
