package Controller;

import java.awt.event.*;
import java.awt.*;
import javax.swing.*;

import display.Menu_main;
import maps.Map;
import objects.Monkey;

public class IHM_ControllerSimu_P extends JFrame implements MouseMotionListener, MouseListener, IController  {
	// ATTRIBUTES
    private static JFrame graphFrame;
    private static JPanel graph;
	private BBoard_State bbs;
	double mouseX, mouseY;
	
	public IHM_ControllerSimu_P(BBoard_State bbs)
	{
		mouseX = 0;
		mouseY = 0;
		this.bbs = bbs;
		
		bbs = new BBoard_State();
		
		// init IHM components
		graphFrame = new JFrame();
        graphFrame.setTitle("Balance Board Simulation");
        graphFrame.setSize(600, 800);
        graphFrame.setResizable(false);
        
        graph = new JPanel()
        {
            public void paintComponent(Graphics graphics)
            {
                graphics.clearRect(0, 0, 600, 800);
                graphics.fillRect(0, 0, 600, 800);
                graphics.setColor(Color.WHITE);
                graphics.drawLine(0, 400, 600, 400);
                graphics.drawLine(300, 0, 300, 800);
                
                graphics.setColor(Color.RED);
                graphics.fillOval((int)( mouseX * 600+300-15), (int)(mouseY * 800+400-15), 30, 30);
            }
        };
        
        graph.setDoubleBuffered(true);
        graphFrame.add(graph);
        graphFrame.setVisible(true);
        graphFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
        graph.addMouseMotionListener(this);
        graph.addMouseListener(this);
	}
	
	private void updateInfos()
	{
		graph.repaint();
	}
	
	public static void main(String[] args) {
		BBoard_State bbs = new BBoard_State();
		//IHM_ControllerSimu_P fuck = new IHM_ControllerSimu_P(bbs);
	}

	@Override
	public void mouseDragged(MouseEvent arg0) {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
		mouseX = arg0.getX() - 300;
		mouseX /= 600;
		mouseY = arg0.getY() - 400;
		mouseY /= 800;
		bbs.xWeight = mouseX;
		bbs.yWeight = mouseY;
		updateInfos();
	}

	@Override
	public void mouseMoved(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseClicked(MouseEvent e) {
	       
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		bbs.jumping = false;
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		bbs.jumping = false;
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		mouseX = 0;
		mouseY = 0;
		bbs.xWeight = mouseX;
		bbs.yWeight = mouseY;
		bbs.jumping = true;
		updateInfos();
	}

	@Override
	public boolean autoConnect() {
		// TODO Auto-generated method stub
		return true;
	}
}
