package objects;

public class Skate {
	public float speedCoef;
	public boolean boosted;
	public float boost;
	public Skate(float speedCoef)
	{
		boost = 0;
		this.speedCoef = speedCoef;
		boosted = false;
	}
	
	public float getSpeedCoef()
	{
		return speedCoef + boost;
	}
}
