package objects;

public class Monkey {
	public double angleSide, angleFront, angleOrientation;
	public float posX, posY, posZ;
	public float speedX, speedY;
	public float maxSpeed; // case par seconde
	public float maxAngleSide,maxAngleFront,maxAngleOrientation;
	public boolean alive,win;
	public String name;
	private Skate m_skate;
	public boolean inTheAir;
	public Object _visualRemove;
	
	public Monkey(String name)
	{
		alive = true;
		win = false;
		maxAngleSide = 0.261799f;
		maxAngleFront = 0.261799f;
		maxAngleOrientation = 0.17f;
		maxSpeed = 5f;
		angleSide = 0;
		angleFront = 0;
		angleOrientation = 0;
		posX = 0;
		posY = 0;
		posZ = 0;
		speedX = 0;
		speedY = 0;
		inTheAir = false;
		this.name = name;
		_visualRemove = null;
		
		m_skate = new Skate(1f);
	}
	
	public Skate getSkate()
	{
		return m_skate;
	}
}