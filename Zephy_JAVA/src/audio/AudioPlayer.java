package audio;

import java.io.File;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

import javafx.scene.image.Image;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.util.Duration;
import sun.applet.Main;

public class AudioPlayer {
static Media m_banana = new Media(new File("sounds/banana.mp3").toURI().toString());
static Media m_fire = new Media(new File("sounds/fire_death.mp3").toURI().toString());
static Media m_noyade = new Media(new File("sounds/splash.mp3").toURI().toString());
static Media m_ridin = new Media(new File("sounds/ridin.mp3").toURI().toString());
static Media m_jump = new Media(new File("sounds/jump.mp3").toURI().toString());
static Media m_victory = new Media(new File("sounds/win.mp3").toURI().toString());
static MediaPlayer w_playerBanana = new MediaPlayer(m_banana);
static MediaPlayer w_playerFire = new MediaPlayer(m_fire);
static MediaPlayer w_playerNoyade = new MediaPlayer(m_noyade);
static MediaPlayer w_playerRidin = new MediaPlayer(m_ridin);
static MediaPlayer w_playerJump = new MediaPlayer(m_jump);
static MediaPlayer w_playerVictory = new MediaPlayer(m_victory);

public static void banana()
{
	w_playerBanana.setVolume(0.2);
	w_playerBanana.seek(Duration.ZERO);
	w_playerBanana.play();
}

public static void fire_death()
{
	w_playerFire.setVolume(0.9);
	w_playerFire.seek(Duration.ZERO);
	w_playerFire.play();
}

public static void noyade() {
	w_playerNoyade.setVolume(0.9);
	w_playerNoyade.seek(Duration.ZERO);
	w_playerNoyade.play();
}

public static void ridin() {
	w_playerRidin.setVolume(0.4);
	w_playerRidin.seek(Duration.ZERO);
	w_playerRidin.play();
	
}
public static void jump() {
	w_playerJump.setVolume(0.9);
	w_playerJump.seek(Duration.ZERO);
	w_playerJump.play();
	
}

public static void win() {
	w_playerRidin.stop();
	w_playerVictory.setVolume(0.5);
	w_playerVictory.seek(Duration.ZERO);
	w_playerVictory.play();
}

}
