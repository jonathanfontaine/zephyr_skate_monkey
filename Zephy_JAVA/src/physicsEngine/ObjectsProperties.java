package physicsEngine;

import audio.AudioPlayer;
import maps.eElement;
import objects.Monkey;
import objects.Skate;
import objects.Z_Object;

public class ObjectsProperties {
	public static void affect(Z_Object obj, Monkey m)
	{
		switch (obj.type) {
		case banana:
			AudioPlayer.banana();
			m._visualRemove = obj.visualObject;
			if(!m.getSkate().boosted)
			{
				new Thread(() -> {
					_boostSkate(m.getSkate(),0.5f,3000);
				}).start();
			}
			break;
		default:
			break;
		}
	}
	
	private static void _boostSkate(Skate s,float boost, int duration)
	{
		s.boosted = true;
		s.boost = boost;
		try {
			Thread.sleep(duration);
		} catch (InterruptedException e) {
			s.boosted = false;
			e.printStackTrace();
		}
		s.boost = 0;
		s.boosted = false;
	}
}
