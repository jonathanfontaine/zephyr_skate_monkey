package physicsEngine;

import Controller.BBoard_State;
import audio.AudioPlayer;
import objects.Monkey;

public class PhysicsEffects {
	public static void noyade(Monkey m)
	{
		new Thread(() -> {
		    _noyade(m);
		}).start();
	}
	
	private static void _noyade(Monkey m)
	{
		for(int i=0; i<30; i++)
		{	
			m.posZ -= 0.05f;
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public static void brulure(Monkey m) {
		new Thread(() -> {
		    _brulure(m);
		}).start();
	}

	private static void _brulure(Monkey m) {
		for(int i=0; i<50; i++)
		{	
			m.angleOrientation += 0.1f;
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		m.angleOrientation = - (Math.PI/2);
		m.posZ += 0.5;
		m.angleSide =  (Math.PI/2.3);
	}

	public static void win(Monkey m) {
		new Thread(() -> {
		    _win(m);
		}).start();
	}

	private static void _win(Monkey m) {
		for(int i=0; i<30; i++)
		{	
			m.posZ += 0.07f;
			m.angleOrientation -= Math.PI/2/30;
			m.angleSide -=  Math.PI/12/30;
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		while(m.win)
		{	
			for(int i=0; i<10; i++)
			{	
				m.posZ += 0.1f;
				try {
					Thread.sleep(50);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			for(int i=0; i<10; i++)
			{	
				m.posZ -= 0.1f;
				try {
					Thread.sleep(50);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
	
	public static void jump(Monkey m, BBoard_State ai_bbs)
	{
		new Thread(() -> {
		    _jump(m,ai_bbs);
		}).start();
	}
	
	private static void _jump(Monkey m,BBoard_State ai_bbs) {
		
		float maxSpeedPerSleep = m.maxSpeed / 33;
		float angleFrontInit = 0.20f;
		m.angleFront = angleFrontInit;
		m.inTheAir = true;
		for(int i=0; i<15; i++)
		{	
			m.posZ += 0.1f;
			//m.posX += m.speedX*maxSpeedPerSleep*Math.abs(m.speedY) *0.75;
			//m.posY -= m.speedY*maxSpeedPerSleep * 0.75;
			m.angleFront -= angleFrontInit/(float)15;
			try {
				Thread.sleep(33);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		m.angleFront = -angleFrontInit;
		for(int i=0; i<15; i++)
		{	
			m.posZ -= 0.1f;
			//m.posX += m.speedX*maxSpeedPerSleep*Math.abs(m.speedY);
			//m.posY -= m.speedY*maxSpeedPerSleep;
			m.angleFront += angleFrontInit/(float)15;
			try {
				Thread.sleep(33);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		m.angleFront = 0f;
		m.posZ = 0;
		ai_bbs.jumping = false;
		m.inTheAir = false;
	}
}
