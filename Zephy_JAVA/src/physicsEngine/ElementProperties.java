package physicsEngine;

import audio.AudioPlayer;
import maps.eElement;
import objects.Monkey;

public class ElementProperties {
	public static boolean isBlocking(eElement elem)
	{
		return (elem == eElement.border || elem == eElement.wood || elem == eElement.tree || elem == eElement.rockBlock);
	}
	public static void affect(eElement elem, Monkey m)
	{
		m.getSkate().speedCoef = 1f;
		m.posZ = 0;
		// water
		if(elem == eElement.grass)
		{
			m.getSkate().speedCoef = 0.85f;
		}
		else if(elem == eElement.water)
		{
			AudioPlayer.noyade();
			m.posX -= m.posX - (int)m.posX;
			m.posY -= m.posY - (int)m.posY;
			m.posX += 0.5;
			m.posY += 0.5;
			m.alive = false;
			PhysicsEffects.noyade(m);
		}
		else if(elem == eElement.fire)
		{
			AudioPlayer.fire_death();
			m.posX -= m.posX - (int)m.posX;
			m.posY -= m.posY - (int)m.posY;
			m.posX += 0.5;
			m.posY += 0.5;
			m.alive = false;
			PhysicsEffects.brulure(m);
		}
		else if(elem == eElement.arrival)
		{
			AudioPlayer.win();
			m.posX -= m.posX - (int)m.posX;
			m.posY -= m.posY - (int)m.posY;
			m.posX += 0.5;
			m.posY += 0.5;
			m.win = true;
			PhysicsEffects.win(m);
		}
		else if(elem == eElement.sand)
		{
			m.getSkate().speedCoef = 0.5f;
		}
		else if(elem == eElement.bridge)
		{
			m.posZ = (float) 0.13;
		}
	}
}
