package physicsEngine;

import Controller.BBoard_State;
import maps.Case;
import maps.Map;
import maps.eElement;
import objects.Monkey;

public class PhysicsEngine {
	private Map myMap;
	private Monkey myMonkey;
	private BBoard_State myBBoard;
	private boolean m_pause;
	
	public PhysicsEngine(Map m, Monkey mo, BBoard_State b)
	{
		myBBoard = b;
		myMap = m;
		myMonkey = mo;
		init();
	}
	
	public void init()
	{
		myMonkey.posX = myMap.mapWidth / 2;
		myMonkey.posY = 1.5f;
	}
	
	public void start()
	{
		m_pause = false;
		new Thread(() -> {
		    try {
				computePhysics();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}).start();
	}
	
	public void stop()
	{
		m_pause = true;
	}
	
	private void computePhysics() throws InterruptedException
	{
		while(myMonkey.alive && !myMonkey.win && !m_pause)
		{	
			int stepSide = 0;
			int stepFront = 0;
			if((myMonkey.posX - (int)myMonkey.posX )>0.5)
			{
				stepSide = 1;
			}
			if((myMonkey.posY - (int)myMonkey.posY)>0.5)
			{
				stepFront = 1;
			}
			// ELEMENT AFFECTATION
			Case w_currentCase = myMap.getCaseAt((int)myMonkey.posX + stepSide, (int)myMonkey.posY + stepFront);
			if(!myMonkey.inTheAir)
			{
				ElementProperties.affect(w_currentCase.element, myMonkey);
			}
			ObjectsProperties.affect(w_currentCase.object, myMonkey);
			float futureX,futureY;
			futureX = myMonkey.posX;
			futureY = myMonkey.posY;
			// compute position
			float maxSpeedPerSleep = myMonkey.maxSpeed / 33;
			if(!myBBoard.jumping)
			{
				futureY -= (myBBoard.yWeight*2)*maxSpeedPerSleep*myMonkey.getSkate().getSpeedCoef()*(1+(Math.abs(myBBoard.yWeight*3)));
				futureX += (myBBoard.xWeight*2)*maxSpeedPerSleep*myMonkey.getSkate().getSpeedCoef();
			}
			else
			{
				futureX += (myMonkey.speedX*1.5)*maxSpeedPerSleep*Math.abs(myMonkey.speedY)*(1+myMonkey.getSkate().boost);
				futureY -= (myMonkey.speedY*1.5)*maxSpeedPerSleep*(1+(Math.abs(myMonkey.speedX*1.5)))*(1+myMonkey.getSkate().boost);
			}

			// jump management
			if(!myMonkey.inTheAir && !myBBoard.jumping && myBBoard.xWeight != 0 && myBBoard.yWeight != 0)
			{
				myMonkey.speedX = (float) (myBBoard.xWeight*2);
				myMonkey.speedY = (float) (myBBoard.yWeight*2);
			}
			if(!myMonkey.inTheAir && myBBoard.jumping)
			{
				PhysicsEffects.jump(myMonkey,myBBoard);
			}
			int frontDirection = 1;
			if(myBBoard.yWeight > 0)
			{
				frontDirection = -1;
			}
			// compute angles
			if(!myBBoard.jumping)
			{
				myMonkey.angleSide = - (float) ((myBBoard.xWeight*2)*myMonkey.maxAngleSide);
				//myMonkey.angleFront = (float) ((myBBoard.yWeight*2)*myMonkey.maxAngleFront);
				myMonkey.angleOrientation = - (float) ((myBBoard.xWeight*2)*myMonkey.maxAngleOrientation)*frontDirection;
			}
			// check position with environment
			// FRONT CHECK
			eElement frontElement = myMap.getCaseAt((int)futureX, Math.round(futureY)+frontDirection).element;
			if( (futureX - (int)futureX)>0.5)
			{
				frontElement = myMap.getCaseAt((int)futureX+1, Math.round(futureY)+frontDirection).element;
			}
			if(!ElementProperties.isBlocking(frontElement))
			{
				myMonkey.posY = futureY;
			}
			// SIDE CHECK
			int side =1;
			int sideDirectionL = 0;
			int sideDirectionR = 1;
			if(myBBoard.xWeight > 0.5)
			{
				side = -1;
				sideDirectionL = -1;
				sideDirectionR = 0;
			}
			eElement sideElement = myMap.getCaseAt((int)(futureX)+sideDirectionL, Math.round(futureY)+1).element;
			if( (futureX - (int)futureX)<0.5)
			{
				sideElement = myMap.getCaseAt((int)futureX+sideDirectionR, Math.round(futureY)+1).element;
			}
			if(!ElementProperties.isBlocking(sideElement))
			{
				myMonkey.posX = futureX;
			}
			Thread.sleep(33);
		}
	}
}
