package processing.box;


public abstract class ProcessingBox extends Thread
{
	// MEMBERS
	public ObjectCarrier[] inputs;
	public ObjectCarrier[] outputs;
	protected int nbInput;
	protected int nbOutput;
	protected int sleep;
	public String type;
	protected volatile boolean toRun;
	protected Object mainObject;
	
	// CONSTRUCTOR
	public ProcessingBox()
	{
		nbInput = 0;
		nbOutput = 0;
		toRun = false;
		sleep = 1000;
		type = "No name";
		mainObject = new Object();
	}
	
	// settings of the derived class of processing box
	protected void set(int _nbInput, int _nbOutPut, int threadFrequency, String _type)
	{
		nbInput = _nbInput;
		inputs = new ObjectCarrier[nbInput];
		// init
		for (int i=0; i<nbInput; i++)
		{
			inputs[i] = new ObjectCarrier();
		}
		nbOutput = _nbOutPut;
		outputs = new ObjectCarrier[nbOutput];
		for (int i=0; i<nbOutput; i++)
		{
			outputs[i] = new ObjectCarrier();
		}
		sleep = 1000/threadFrequency;
		type = _type;
	}
	
	// FUNCTIONS
	protected void sendToOutput(int numOutput, Object obj)
	{
		if (numOutput<nbOutput && numOutput>=0)
		{
			outputs[numOutput].set(obj);
		}
		else
		{
			System.out.println("ERROR : invalide output to send.");
		}
	}
	
	// affect main object
	public void affect(Object o)
	{
		mainObject = o;
	}
	// get Main object
	public Object getMO()
	{
		return mainObject;
	}
	
	// main function of the active object
	protected abstract void _process();
	
	@Override
    public void run() {
		System.out.println(type + " started");
		toRun = true;
        while(toRun) {
            // Traitement
			_process();
        	// gestion du thread
            try
            {
                Thread.sleep(sleep);
            } 
            catch (InterruptedException ex)
            {
                Thread.currentThread().interrupt();
                break; 
            }
        }
    }
	
	// mettre en pause le thread
	public void terminate()
	{
		toRun = false;
		System.out.println("INFO : " + type + " terminated");
	}
	
	// establish a connection betwen 2 processing boxes
	public boolean connect(int numOutput, ProcessingBox receiver, int numInput)
	{
		// thread should not be started
		if(toRun)
		{
			System.out.print("ERROR : cant hot connect " + type +".");
			return false;
		}
		// types of object must match
		/*
		if(outputs[numOutput].getClass() != receiver.inputs[numInput].getClass())
		{
			System.out.print("ERROR : class does not match.");
			return false;
		}
		*/
		// establish connection
		// same referenced object
		outputs[numOutput] = receiver.inputs[numInput];
		return true;
	}
	
	public void initialize()
	{
		
	}
}

