package processing.box;

/** carry an object in a connection */
public class ObjectCarrier {
	private Object obj;
	public synchronized Object get()
	{
		return obj;
	}
	public synchronized void set(Object val)
	{
		obj = val;
	}
}

