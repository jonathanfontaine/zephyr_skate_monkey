package maps;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;

import objects.eObject;

public class Map {
	// MEMBERS
	public int blockSize, mapWidth;
	public String name;
	public MapSegment currentSegment;
	// CONSTRUCTROS
	public Map(String fileName)
	{
		blockSize = 50;
		String mbp = "maps/";
		// open mapdef file
		BufferedReader br;
		try {
			String line;
			br = new BufferedReader(new FileReader(mbp+fileName+".txt"));
			// get map name
			name = br.readLine();
			// get struct file dimensions
			line = br.readLine();
			mapWidth = Integer.parseInt(line);
			// get all segments
			ArrayList<String> segments = new ArrayList<String>();
			line = br.readLine();
			while(!line.equals("END"))
			{
				segments.add(line);
				line = br.readLine();
			}
			// createSegment
			currentSegment = new MapSegment(segments);
		    br.close();
		}
		catch(Exception e)
		{
			System.out.println("Problem with file " + fileName);
		}
	}
	
	public Case getCaseAt(int x, int y)
	{
		return currentSegment.getCasetAt(x,y);
	}
	
	// METHODES
	public String toText()
	{
		return currentSegment.toText();
	}
	
	// MAIN
	public static void main(String[] args) {
		Map mIsland = new Map("monkey_island");
		System.out.println(mIsland.toText());
	}
}
