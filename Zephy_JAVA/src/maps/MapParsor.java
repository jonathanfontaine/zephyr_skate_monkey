package maps;

import java.util.ArrayList;

import common.Pair;
import objects.eObject;

public class MapParsor {
	
	public static eElement charToElement(char c)
	{
		switch (c)
		{
		case	'b': return eElement.border;
		case	'g': return eElement.grass;
		case	'f': return eElement.fire;
		case	'h': return eElement.hole;
		case	'w': return eElement.wood;
		case	'a': return eElement.arrival;
		case    'W': return eElement.water;
		case    's': return eElement.sand;
		case	't': return eElement.tree;
		case	'B': return eElement.bridge;
		case	'r': return eElement.rock;
		case	'R': return eElement.rockBlock;
		case	'G': return eElement.ground;
		default : return eElement.none;
		}
	}
	
	public static eObject charToObject(char c)
	{
		switch (c)
		{
		case	'b': return eObject.banana;
		default : return eObject.none;
		}
	}
	
	public static char ElementToChar(eElement e)
	{
		switch (e)
		{
		case	border: return 'b';
		case	grass: return 'g';
		case	fire: return 'f';
		case	hole: return 'h';
		case	wood : return 'w';
		case	arrival: return 'a';
		case    water: return 'W';
		case 	sand : return 's';
		case 	tree : return 't';
		case 	bridge : return 'B';
		case 	rock : return 'r';
		case	ground : return 'G';
		case	rockBlock : return 'R';
		default : return 'n';
		}
	}
	
	public static Pair<Integer,String> parsePortionLine(String s)
	{
		Pair<Integer,String> answer = new Pair<Integer,String>();
		String[] splitted = s.split(" ");
		answer.first = Integer.parseInt( splitted[1] );
		answer.second = splitted[0];
		return answer;
	}
	
	public static ArrayList<Pair<eObject,Integer>> parseObjects(String s)
	{
		ArrayList<Pair<eObject,Integer>> answer = new ArrayList<Pair<eObject,Integer>>();
		String[] splitted = s.split(",");
		for(int i = 0; i<splitted.length; i++)
		{
			Pair<eObject,Integer> w_objectData = new Pair<eObject,Integer>();
			w_objectData.first = charToObject( splitted[i].charAt(0) );
			w_objectData.second = Integer.parseInt( splitted[i].substring(1) );
			answer.add(w_objectData);
		}
		return answer;
	}
}
