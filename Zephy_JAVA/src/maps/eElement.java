package maps;

public enum eElement {
	border,
	grass,
	fire,
	hole,
	wood,
	water,
	arrival,
	sand,
	bridge,
	tree,
	rock,
	ground,
	rockBlock,
	none;
	
}
