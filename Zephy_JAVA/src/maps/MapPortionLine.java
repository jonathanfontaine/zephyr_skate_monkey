package maps;

import java.util.ArrayList;

import common.Pair;
import objects.Z_Object;
import objects.eObject;

public class MapPortionLine {
	// MEMBERS
	public int size;
	public Case[] cases;
	// CONSTRUCTORS
	public MapPortionLine(int portionSize)
	{
		size = portionSize;
		cases = new Case[size];
		for(int i=0; i<size; i++)
		{
			cases[i] = new Case(eElement.none,eObject.none);
		}
	}
	public MapPortionLine(String elements)
	{
		size = elements.length();
		cases = new Case[size];
		for(int i=0; i<size; i++)
		{
			cases[i] = new Case(MapParsor.charToElement(elements.charAt(i)),eObject.none);
		}
	}
	public MapPortionLine(String elements, ArrayList<Pair<eObject,Integer>> objects)
	{
		size = elements.length();
		cases = new Case[size];
		for(int i=0; i<size; i++)
		{
			cases[i] = new Case(MapParsor.charToElement(elements.charAt(i)),eObject.none);
		}
		for(Pair<eObject,Integer> objCoord : objects)
		{
			cases[objCoord.second].object.type = objCoord.first;
		}
	}
	public String toText()
	{
		String s = "";
		for(int i=0;i<size; i++)
		{
			s+= MapParsor.ElementToChar(cases[i].element);
		}
		return s;
	}
}
