package maps;

import objects.Z_Object;
import objects.eObject;

public class Case {
	public eElement element;
	public Z_Object object;	
	public Case (eElement elem, eObject obj)
	{
		element = elem;
		object = new Z_Object(obj);
	}
}
