package maps;

import java.util.ArrayList;
import common.Pair;
import objects.eObject;

public class MapSegment {
	// MEMBERS
	public ArrayList<Pair<MapPortionLine, Integer>> portions;
	//CONSTRUCTORS
	public MapSegment(ArrayList<String> ai_portions)
	{
		// full segment lines
		portions = new ArrayList<Pair<MapPortionLine, Integer>>();
		Pair<Integer,String> result;
		for(int i= ai_portions.size() -1; i>=0; i--)
		{
			result = MapParsor.parsePortionLine(ai_portions.get(i));
			String[] tmp = ai_portions.get(i).split(" ");
			if(tmp.length == 3)
			{
				ArrayList<Pair<eObject,Integer>> objects = MapParsor.parseObjects(tmp[2]);
				portions.add(new Pair<MapPortionLine, Integer>(new MapPortionLine(result.second,objects),result.first));
			}
			else
			{
				portions.add(new Pair<MapPortionLine, Integer>(new MapPortionLine(result.second),result.first));
			}
		}
	}
	public String toText() {
		String s = "";
		for(Pair<MapPortionLine, Integer> portion : portions)
		{
			s+= portion.first.toText() + " " + portion.second + '\n';
		}
		return s;
	}
	public Case getCasetAt(int x, int y) {
		int lineCursor = 0;
		for(int i=0; i<portions.size(); i++)
		{
			if(lineCursor + portions.get(i).second > y)
			{
				return portions.get(i).first.cases[x];
			}
			else
			{
				lineCursor += portions.get(i).second;
			}
		}
		return null;
	}
}
